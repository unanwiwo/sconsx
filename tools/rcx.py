# -*- coding: utf-8 -*-

# This file is part of SConsX.
#
# SConsX is an extension of the SCons - a software construction tool.
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import utils, os, SCons

def emit_windows(target, source, env):
    source = env.Cpp(os.path.join("$SCONSX_RCX_BUILD_PATH", utils.root(source)), source, SCONSX_CPP_PREFIX="$SCONSX_RCX_PREFIX", SCONSX_CPP_SUFFIX="$SCONSX_RCX_SUFFIX")
    return [os.path.join("$SCONSX_RCX_OBJECT_BUILD_PATH", utils.adjustixes(source, "${OBJPREFIX}", "${OBJSUFFIX}"))], source

def emit_dummy(target, source, env):
    return [], []

def generate_windows(env):
    env.Tool("cpp")

    static_obj_builder, shared_obj_builder = SCons.Tool.createObjBuilders(env)
    static_obj_builder.add_action("$SCONSX_RCX_SUFFIX", SCons.Action.Action("$RCCOM", "$RCCOMSTR"))
    shared_obj_builder.add_action("$SCONSX_RCX_SUFFIX", SCons.Action.Action("$RCCOM", "$RCCOMSTR"))
    static_obj_builder.add_action("$SCONSX_RCX_SRC_SUFFIX", None)
    shared_obj_builder.add_action("$SCONSX_RCX_SRC_SUFFIX", None)
    static_obj_builder.add_emitter("$SCONSX_RCX_SRC_SUFFIX", emit_windows)
    shared_obj_builder.add_emitter("$SCONSX_RCX_SRC_SUFFIX", emit_windows)

def generate_dummy(env):
    static_obj_builder, shared_obj_builder = SCons.Tool.createObjBuilders(env)
    static_obj_builder.add_action("$SCONSX_RCX_SRC_SUFFIX", None)
    shared_obj_builder.add_action("$SCONSX_RCX_SRC_SUFFIX", None)
    static_obj_builder.add_emitter("$SCONSX_RCX_SRC_SUFFIX", emit_dummy)
    shared_obj_builder.add_emitter("$SCONSX_RCX_SRC_SUFFIX", emit_dummy)

def generate(env):
    env.SetDefault(
        SCONSX_RCX_BUILD_PATH=os.path.join("$SCONSX_BUILD_PATH", "resources", "$SCONSX_HOST", "$SCONSX_BUILD_MODE"),
        SCONSX_RCX_OBJECT_BUILD_PATH="$SCONSX_OBJECT_BUILD_PATH",

        SCONSX_RCX_PREFIX="rcx_",
        SCONSX_RCX_SUFFIX=".rc",
        SCONSX_RCX_SRC_SUFFIX=".rcx",
    )

    if env["SCONSX_WINDOWS_BIT"]:
        generate_windows(env)
    else:
        generate_dummy(env)

def exists(env):
    return True
