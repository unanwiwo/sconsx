# -*- coding: utf-8 -*-

# This file is part of SConsX.
#
# SConsX is an extension of the SCons - a software construction tool.
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os, SCons

def generate(env):
    env.SetDefault(
        SCONSX_INNO="iscc",
        SCONSX_INNO_FLAGS="${SCONSX_INNO_CONSTRUCT_DEFINES(__env__)}",

        SCONSX_INNO_COM="$SCONSX_INNO $SCONSX_INNO_FLAGS /O${TARGET.dir} /F${SCONSX_INNO_REMOVE_LAST_SUFFIX(TARGET.file)} $SOURCE",
        SCONSX_INNO_COM_STR="",

        SCONSX_INNO_PREFIX="",
        SCONSX_INNO_SUFFIX=".exe",
        SCONSX_INNO_SRC_SUFFIX=".iss",

        SCONSX_INNO_CONSTRUCT_DEFINES=lambda env: ' '.join(['"/D{0}"'.format(env.subst(define)) for define in env["SCONSX_INNO_DEFINES"]]),
        SCONSX_INNO_REMOVE_LAST_SUFFIX=lambda file: os.path.splitext(str(file))[0],

        SCONSX_INNO_VERSION="$SCONSX_VERSION",
        SCONSX_INNO_SOURCE_PATH="$SCONSX_BINARY_BUILD_DIR",

        SCONSX_INNO_NAME="",
        SCONSX_INNO_PUBLISHER="",
        SCONSX_INNO_URL="",
        SCONSX_INNO_MAIN_EXE_NAME="",

        SCONSX_INNO_DEFINES=[
            "INNO_VERSION=$SCONSX_INNO_VERSION",
            "INNO_SOURCE_PATH=$SCONSX_INNO_SOURCE_PATH",
            "INNO_NAME=$SCONSX_INNO_NAME",
            "INNO_PUBLISHER=$SCONSX_INNO_PUBLISHER",
            "INNO_URL=$SCONSX_INNO_URL",
            "INNO_MAIN_EXE_NAME=$SCONSX_INNO_MAIN_EXE_NAME",
        ],
    )

    env.Append(
        BUILDERS={
            "Inno": SCons.Builder.Builder(
                action=SCons.Action.Action("$SCONSX_INNO_COM", "$SCONSX_INNO_COM_STR"),
                # TODO
                #source_scanner=SCons.Scanner.Scanner(
                #    function=lambda node, *args: pass,
                #    skeys="$SCONSX_INNO_SRC_SUFFIX"
                #),
                prefix="$SCONSX_INNO_PREFIX",
                suffix="$SCONSX_INNO_SUFFIX",
                src_suffix="$SCONSX_INNO_SRC_SUFFIX",
            ),
        },
    )

def exists(env):
    return env.WhereIs("iscc")
