# -*- coding: utf-8 -*-

# This file is part of SConsX.
#
# SConsX is an extension of the SCons - a software construction tool.
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import utils, os, SCons

stylesheets = {
    "qt": ("sconsx/data/qt_header.xsl", "sconsx/data/qt_source.xsl"),
}

def extract(target, source, env):
    env.Xslt(os.path.join("$SCONSX_SETTINGS_BUILD_PATH", utils.root(source)), [source[0], os.path.join("#", "${SCONSX_SETTINGS_HEADER_STYLESHEET}")], SCONSX_XSLT_PREFIX="$SCONSX_SETTINGS_PREFIX", SCONSX_XSLT_SUFFIX="$SCONSX_SETTINGS_HEADER_SUFFIX")
    node = env.Xslt(os.path.join("$SCONSX_SETTINGS_BUILD_PATH", utils.root(source)), [source[0], os.path.join("#", "${SCONSX_SETTINGS_SOURCE_STYLESHEET}")], SCONSX_XSLT_PREFIX="$SCONSX_SETTINGS_PREFIX", SCONSX_XSLT_SUFFIX="$SCONSX_SETTINGS_SOURCE_SUFFIX")
    return [os.path.join("$SCONSX_OBJECT_BUILD_PATH", utils.adjustixes(node, "${OBJPREFIX}", "${OBJSUFFIX}"))], node

def generate(env):
    env.Tool("xslt")

    env.SetDefault(
        SCONSX_SETTINGS_STYLESHEET="",
        SCONSX_SETTINGS_HEADER_STYLESHEET="",
        SCONSX_SETTINGS_SOURCE_STYLESHEET="",

        SCONSX_SETTINGS_BUILD_PATH=os.path.join("$SCONSX_BUILD_PATH", "settings"),

        SCONSX_SETTINGS_PREFIX="",
        SCONSX_SETTINGS_HEADER_SUFFIX=".hpp",
        SCONSX_SETTINGS_SOURCE_SUFFIX=".cpp",
        SCONSX_SETTINGS_SRC_SUFFIX=".xsds",
    )

    env.PrependUnique(
        CPPPATH=["$SCONSX_SETTINGS_BUILD_PATH"],
    )

    if env["SCONSX_SETTINGS_STYLESHEET"]:
        env["SCONSX_SETTINGS_HEADER_STYLESHEET"], env["SCONSX_SETTINGS_SOURCE_STYLESHEET"] = stylesheets[env["SCONSX_SETTINGS_STYLESHEET"]]

    static_obj_builder, shared_obj_builder = SCons.Tool.createObjBuilders(env)
    static_obj_builder.add_action("$SCONSX_SETTINGS_SRC_SUFFIX", SCons.Defaults.CXXAction)
    shared_obj_builder.add_action("$SCONSX_SETTINGS_SRC_SUFFIX", SCons.Defaults.CXXAction)
    static_obj_builder.add_emitter("$SCONSX_SETTINGS_SRC_SUFFIX", extract)
    shared_obj_builder.add_emitter("$SCONSX_SETTINGS_SRC_SUFFIX", extract)

def exists(env):
    return True
