# -*- coding: utf-8 -*-

# This file is part of SConsX.
#
# SConsX is an extension of the SCons - a software construction tool.
# Copyright (C) 2007-2017 Yanis Kurganov <yanis.kurganov@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import utils, os, re, SCons

moc_qobject_re = re.compile(r"\bQ_OBJECT\b")
qrc_include_re = re.compile(r"<file[^>]*>([^<]*)</file>", re.MULTILINE)

def core_module(env):
    env.AppendUnique(
        CPPDEFINES=["QT_CORE_LIB"],
        CPPPATH=[os.path.join("$SCONSX_QT_CPP_PATH", "QtCore")],
        LIBS={
            "release": ["Qt5Core"],
            "debug": ["Qt5Cored"],
        }[env["SCONSX_QT_BUILD_MODE"]],
    )

def gui_module(env):
    env.AppendUnique(
        CPPDEFINES=["QT_GUI_LIB"],
        CPPPATH=[os.path.join("$SCONSX_QT_CPP_PATH", "QtGui")],
        LIBS={
            "release": ["Qt5Gui"],
            "debug": ["Qt5Guid"],
        }[env["SCONSX_QT_BUILD_MODE"]],
    )

def widgets_module(env):
    env.AppendUnique(
        CPPDEFINES=["QT_WIDGETS_LIB"],
        CPPPATH=[os.path.join("$SCONSX_QT_CPP_PATH", "QtWidgets")],
        LIBS={
            "release": ["Qt5Widgets"],
            "debug": ["Qt5Widgetsd"],
        }[env["SCONSX_QT_BUILD_MODE"]],
    )

def opengl_module(env):
    env.AppendUnique(
        CPPDEFINES=["QT_OPENGL_LIB"],
        CPPPATH=[os.path.join("$SCONSX_QT_CPP_PATH", "QtOpenGL")],
        LIBS={
            "release": ["Qt5OpenGL"],
            "debug": ["Qt5OpenGLd"],
        }[env["SCONSX_QT_BUILD_MODE"]],
    )

def network_module(env):
    env.AppendUnique(
        CPPDEFINES=["QT_NETWORK_LIB"],
        CPPPATH=[os.path.join("$SCONSX_QT_CPP_PATH", "QtNetwork")],
        LIBS={
            "release": ["Qt5Network"],
            "debug": ["Qt5Networkd"],
        }[env["SCONSX_QT_BUILD_MODE"]],
    )

def sql_module(env):
    env.AppendUnique(
        CPPDEFINES=["QT_SQL_LIB"],
        CPPPATH=[os.path.join("$SCONSX_QT_CPP_PATH", "QtSql")],
        LIBS={
            "release": ["Qt5Sql"],
            "debug": ["Qt5Sqld"],
        }[env["SCONSX_QT_BUILD_MODE"]],
    )

def xml_module(env):
    env.AppendUnique(
        CPPDEFINES=["QT_XML_LIB"],
        CPPPATH=[os.path.join("$SCONSX_QT_CPP_PATH", "QtXml")],
        LIBS={
            "release": ["Qt5Xml"],
            "debug": ["Qt5Xmld"],
        }[env["SCONSX_QT_BUILD_MODE"]],
    )

def help_module(env):
    env.AppendUnique(
        CPPPATH=[os.path.join("$SCONSX_QT_CPP_PATH", "QtHelp")],
        LIBS={
            "release": ["Qt5Help"],
            "debug": ["Qt5Helpd"],
        }[env["SCONSX_QT_BUILD_MODE"]],
    )

modules = {
    "core": core_module,
    "gui": gui_module,
    "widgets": widgets_module,
    "opengl": opengl_module,
    "network": network_module,
    "sql": sql_module,
    "xml": xml_module,
    "help": help_module,
}

def no_keywords_feature(env):
    env.AppendUnique(
        CPPDEFINES=["QT_NO_KEYWORDS"],
    )

features = {
    "no_keywords": no_keywords_feature,
}

def moc_emitter(target, source, env):
    for src in (src.sources[0] for src in source[:] if src.sources and src.sources[0] and src.sources[0].get_suffix() in [".cpp"]):
        try:
            hdr = env.File(os.path.splitext(src.path)[0] + ".hpp")
        except SCons.Errors.UserError as e:
            utils.log_error(env, "qt: {0}".format(e))
        else:
            if moc_qobject_re.search(hdr.get_text_contents()):
                moc = env.Moc(os.path.join("$SCONSX_QT_MOC_BUILD_PATH", utils.root(hdr)), hdr, SCONSX_QT_MOC_PREFIX="$SCONSX_QT_MOC_H_PREFIX", SCONSX_QT_MOC_SUFFIX="$SCONSX_QT_MOC_H_SUFFIX")
                source.append(env.Object(os.path.join("$SCONSX_OBJECT_BUILD_PATH", utils.root(moc)), moc))
        if moc_qobject_re.search(src.get_text_contents()):
            env.Moc(os.path.join("$SCONSX_QT_MOC_BUILD_PATH", utils.root(src)), src, SCONSX_QT_MOC_PREFIX="$SCONSX_QT_MOC_C_PREFIX", SCONSX_QT_MOC_SUFFIX="$SCONSX_QT_MOC_C_SUFFIX")
    return target, source

def uic_emitter(target, source, env):
    env.Uic(os.path.join("$SCONSX_QT_UIC_BUILD_PATH", utils.root(source)), source)
    return [], []

def rcc_emitter(target, source, env):
    rcc = env.Rcc(os.path.join("$SCONSX_QT_RCC_BUILD_PATH", utils.root(source)), source)
    return [os.path.join("$SCONSX_OBJECT_BUILD_PATH", utils.adjustixes(rcc, "${OBJPREFIX}", "${OBJSUFFIX}"))], rcc

def generate(env):
    env.SetDefault(
        SCONSX_QT_BIN_PATH=os.path.join("$SCONSX_QT_PATH", "bin"),
        SCONSX_QT_CPP_PATH=os.path.join("$SCONSX_QT_PATH", "include"),
        SCONSX_QT_LIB_PATH=os.path.join("$SCONSX_QT_PATH", "lib"),

        SCONSX_QT_BUILD_MODE="release" if env["SCONSX_QT_USE_ONLY_RELEASE_LIBS"] else env["SCONSX_BUILD_MODE"],

        SCONSX_QT_MOC=os.path.join("$SCONSX_QT_BIN_PATH","moc"),
        SCONSX_QT_UIC=os.path.join("$SCONSX_QT_BIN_PATH", "uic"),
        SCONSX_QT_RCC=os.path.join("$SCONSX_QT_BIN_PATH", "rcc"),
        SCONSX_QT_LUPDATE=os.path.join("$SCONSX_QT_BIN_PATH", "lupdate"),
        SCONSX_QT_LRELEASE=os.path.join("$SCONSX_QT_BIN_PATH", "lrelease"),

        SCONSX_QT_MOC_BUILD_PATH=os.path.join("$SCONSX_BUILD_PATH", "moc", "$SCONSX_HOST", "$SCONSX_BUILD_MODE"),
        SCONSX_QT_UIC_BUILD_PATH=os.path.join("$SCONSX_BUILD_PATH", "forms", "$SCONSX_HOST"),
        SCONSX_QT_RCC_BUILD_PATH=os.path.join("$SCONSX_BUILD_PATH", "resources", "$SCONSX_HOST"),

        SCONSX_QT_MOC_FLAGS="$_CPPDEFFLAGS $_CPPINCFLAGS",
        SCONSX_QT_UIC_FLAGS="",
        SCONSX_QT_RCC_FLAGS="-name ${SOURCE.filebase}",
        SCONSX_QT_LUPDATE_FLAGS="$_CPPINCFLAGS",
        SCONSX_QT_LRELEASE_FLAGS="",

        SCONSX_QT_MOC_COM="$SCONSX_QT_MOC $SCONSX_QT_MOC_FLAGS -o $TARGET $SOURCE",
        SCONSX_QT_UIC_COM="$SCONSX_QT_UIC $SCONSX_QT_UIC_FLAGS -o $TARGET $SOURCE",
        SCONSX_QT_RCC_COM="$SCONSX_QT_RCC $SCONSX_QT_RCC_FLAGS -o $TARGET $SOURCE",
        SCONSX_QT_LUPDATE_COM="${TEMPFILE('$SCONSX_QT_LUPDATE $SCONSX_QT_LUPDATE_FLAGS $SOURCES -ts $TARGET')}",
        SCONSX_QT_LRELEASE_COM="$SCONSX_QT_LRELEASE $SCONSX_QT_LRELEASE_FLAGS $SOURCE -qm $TARGET",

        SCONSX_QT_MOC_COM_STR="",
        SCONSX_QT_UIC_COM_STR="",
        SCONSX_QT_RCC_COM_STR="",
        SCONSX_QT_LUPDATE_COM_STR="",
        SCONSX_QT_LRELEASE_COM_STR="",

        SCONSX_QT_MOC_PREFIX="",
        SCONSX_QT_MOC_SUFFIX="",
        SCONSX_QT_MOC_H_PREFIX="moc_",
        SCONSX_QT_MOC_H_SUFFIX=".cpp",
        SCONSX_QT_MOC_C_PREFIX="",
        SCONSX_QT_MOC_C_SUFFIX=".moc",
        SCONSX_QT_MOC_SRC_SUFFIX="",

        SCONSX_QT_UIC_PREFIX="ui_",
        SCONSX_QT_UIC_SUFFIX=".h",
        SCONSX_QT_UIC_SRC_SUFFIX=".ui",

        SCONSX_QT_RCC_PREFIX="qrc_",
        SCONSX_QT_RCC_SUFFIX=".cpp",
        SCONSX_QT_RCC_SRC_SUFFIX=".qrc",

        SCONSX_QT_LUPDATE_PREFIX="",
        SCONSX_QT_LUPDATE_SUFFIX=".ts",
        SCONSX_QT_LUPDATE_SRC_SUFFIX="",

        SCONSX_QT_LRELEASE_PREFIX="",
        SCONSX_QT_LRELEASE_SUFFIX=".qm",
        SCONSX_QT_LRELEASE_SRC_SUFFIX=".ts",
    )

    env.AppendUnique(
        CPPPATH=["$SCONSX_QT_UIC_BUILD_PATH", "$SCONSX_QT_MOC_BUILD_PATH", "$SCONSX_QT_CPP_PATH"],
        LIBPATH=["$SCONSX_QT_LIB_PATH"],
    )

    if env["SCONSX_RELEASE"]:
        env.AppendUnique(
            CPPDEFINES=["QT_NO_DEBUG"],
        )

    if env["SCONSX_GNUC_BIT"]:
        env.AppendUnique(
            CCFLAGS=["-fPIC"],
            LINKFLAGS=["-Wl,-rpath,$SCONSX_QT_LIB_PATH"],
        )

    for module in env["SCONSX_QT_MODULES"]:
        modules[module](env)

    for feature in env["SCONSX_QT_FEATURES"]:
        features[feature](env)

    env.Append(
        BUILDERS={
            "Moc": SCons.Builder.Builder(
                action=SCons.Action.Action("$SCONSX_QT_MOC_COM", "$SCONSX_QT_MOC_COM_STR"),
                prefix="$SCONSX_QT_MOC_PREFIX",
                suffix="$SCONSX_QT_MOC_SUFFIX",
                src_suffix="$SCONSX_QT_MOC_SRC_SUFFIX",
                single_source=True,
            ),

            "Uic": SCons.Builder.Builder(
                action=SCons.Action.Action("$SCONSX_QT_UIC_COM", "$SCONSX_QT_UIC_COM_STR"),
                prefix="$SCONSX_QT_UIC_PREFIX",
                suffix="$SCONSX_QT_UIC_SUFFIX",
                src_suffix="$SCONSX_QT_UIC_SRC_SUFFIX",
                single_source=True,
            ),

            "Rcc": SCons.Builder.Builder(
                action=SCons.Action.Action("$SCONSX_QT_RCC_COM", "$SCONSX_QT_RCC_COM_STR"),
                source_scanner = SCons.Scanner.Scanner(
                    function=lambda node, *args: qrc_include_re.findall(node.get_text_contents()),
                    skeys="$SCONSX_QT_RCC_SRC_SUFFIX"
                ),
                prefix="$SCONSX_QT_RCC_PREFIX",
                suffix="$SCONSX_QT_RCC_SUFFIX",
                src_suffix="$SCONSX_QT_RCC_SRC_SUFFIX",
                single_source=True,
            ),

            "Lupdate": SCons.Builder.Builder(
                action=SCons.Action.Action("$SCONSX_QT_LUPDATE_COM", "$SCONSX_QT_LUPDATE_COM_STR"),
                prefix="$SCONSX_QT_LUPDATE_PREFIX",
                suffix="$SCONSX_QT_LUPDATE_SUFFIX",
                src_suffix="$SCONSX_QT_LUPDATE_SRC_SUFFIX",
                multi=True,
            ),

            "Lrelease": SCons.Builder.Builder(
                action=SCons.Action.Action("$SCONSX_QT_LRELEASE_COM", "$SCONSX_QT_LRELEASE_COM_STR"),
                prefix="$SCONSX_QT_LRELEASE_PREFIX",
                suffix="$SCONSX_QT_LRELEASE_SUFFIX",
                src_suffix="$SCONSX_QT_LRELEASE_SRC_SUFFIX",
                single_source=True,
            ),
        },
    )

    env.AppendUnique(
        PROGEMITTER=[moc_emitter],
        SHLIBEMITTER=[moc_emitter],
        LIBEMITTER=[moc_emitter],
    )

    static_obj_builder, shared_obj_builder = SCons.Tool.createObjBuilders(env)
    static_obj_builder.add_action("$SCONSX_QT_UIC_SRC_SUFFIX", None)
    shared_obj_builder.add_action("$SCONSX_QT_UIC_SRC_SUFFIX", None)
    static_obj_builder.add_emitter("$SCONSX_QT_UIC_SRC_SUFFIX", uic_emitter)
    shared_obj_builder.add_emitter("$SCONSX_QT_UIC_SRC_SUFFIX", uic_emitter)
    static_obj_builder.add_action("$SCONSX_QT_RCC_SRC_SUFFIX", SCons.Defaults.CXXAction)
    shared_obj_builder.add_action("$SCONSX_QT_RCC_SRC_SUFFIX", SCons.Defaults.CXXAction)
    static_obj_builder.add_emitter("$SCONSX_QT_RCC_SRC_SUFFIX", rcc_emitter)
    shared_obj_builder.add_emitter("$SCONSX_QT_RCC_SRC_SUFFIX", rcc_emitter)

def exists(env):
    return True
